#!/usr/local/bin/gnuplot

if ( ARGC < 4 ) exit error "Expected four inputs!";

# set margins for plot in percentage
Leftmargin      = ARG1
Rightmargin     = ARG2
Topmargin       = ARG3
Bottommargin    = ARG4

### add function/arg noHeader/noTitle
# unset title
# set tmargin at screen 0.
# RatioHeight = 1. + Bottom

RatioWidth      = 1. + Leftmargin + Rightmargin
RatioHeight     = 1. + Topmargin + Bottommargin

# set margins
set lmargin at screen Leftmargin
set rmargin at screen (1. - Rightmargin)
set tmargin at screen (1. - Topmargin)
set bmargin at screen Bottommargin

TotalWidth(Width)   = Width * RatioWidth
TotalHeight(Height)  = Height * RatioHeight


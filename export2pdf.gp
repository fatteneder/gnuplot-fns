#!/usr/local/bin/gnuplot

# if ( ARGC < 1 ) {
    # export error "Missing input: expected file name!"
# } else {
    # fileName = ARG1
# }
# width   = 16.
# height  = 12.
# if ( ARGC > 2 ) width   = ARG2
# if ( ARGC > 3 ) height  = ARG3

# eval(sprintf(\
    # "set terminal cairolatex standalone pdf colortext ps 0.7 fontscale 0.5 size %.1f cm, %.1f cm",\
    # width, height))
#
#
# eval(sprintf(\
    # "set terminal cairolatex input pdf size %.1f cm, %.1f cm",\
    # width, height))
#
# set output fileName
#
# replot
#
# unset output
#
# set terminal qt

setPdfTerm(Width, Height, Fontsize) = sprintf( \
    "set terminal cairolatex standalone pdf colortext linewidth 5 font \",%.1f\" size %.1f cm, %.1f cm",\
    Fontsize, Width, Height )

setPdfTermInput(Width, Height) = sprintf( \
    "set terminal cairolatex input pdf size %.1f cm, %.1f cm",\
    Width, Height )

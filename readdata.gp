readdata(filename) = sprintf( \
  "load '< echo \"\\\$DATA << EOD \" & cat '.'%s'; " . \
  "stats $DATA name \"DATA\" nooutput; " . \
  "print sprintf(\"DATA_blocks = %i read.\", DATA_blocks); ", \
  filename)

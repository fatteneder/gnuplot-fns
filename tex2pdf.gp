MsgGeneratePdf(FileName, OutputDir) = sprintf( \
        "print \"Generating pdf from '%s' in directory '%s' ...\" ", \
        FileName, OutputDir)

tex2pdf(FileName, OutputDir) = sprintf( \
        "eval(MsgGeneratePdf(\"%s\", \"%s\")); " . \
        "system( \"pdflatex -interaction=batchmode -output-directory=%s %s 2>&1 > /dev/null \" )", \
        FileName, OutputDir, OutputDir, FileName )

tex2pdf_verbose(FileName, OutputDir) = sprintf( \
        "eval(MsgGeneratePdf(\"%s\", \"%s\")); " . \
        "system( \" pdflatex -halt-on-error -output-directory=%s '%s' \" )", \
        FileName, OutputDir, OutputDir, FileName )

#!/usr/local/bin/gnuplot

# taken over from https://stackoverflow.com/a/54086937
# usage:
#       set table $Dummy
#           plot ....
#       unset table
#       SemiAutoTicX(Nx)
#       SemiAutoTicY(Ny)
#       replot

Round(n) = gprintf("%.0e",n)
# or alternatively with less approximate tics: 
# Round(n) = gprintf("%.0e",n) + sgn(n)*10**gprintf("%T",n)

SemiAutoTicX(ApproxTicN) = Round((GPVAL_X_MAX - GPVAL_X_MIN)/ApproxTicN)
SemiAutoTicY(ApproxTicN) = Round((GPVAL_Y_MAX - GPVAL_Y_MIN)/ApproxTicN)
